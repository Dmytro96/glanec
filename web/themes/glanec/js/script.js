function format_number(value) {
  if(value < 10) {
    value='0'+value;
  }
  return value;
}

var current_datetime = new Date();

function date() {
  var day = format_number(current_datetime.getDate());
  var month = format_number(current_datetime.getMonth()+1);
  var year = current_datetime.getFullYear();
  return day+"."+month+"."+year;
}

function time() {
  var hours = format_number(current_datetime.getHours());
  var minutes = format_number(current_datetime.getMinutes());
  var seconds = format_number(current_datetime.getSeconds());
  return hours+":"+minutes+":"+seconds;
}

document.getElementById('date').innerHTML = date();
document.getElementById('time').innerHTML = time();